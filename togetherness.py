from datetime import date, timedelta

benBday = date(1977, 1, 24)
erinBday = date(1976, 5, 30)

meeting = date(1999, 9, 15)

# fraction = (x - meeting) / (x - bday1) = (x - meeting) / (x - bday2)
# (x - meeting) * (x - bday2) = (x - meeting) * (x - bday1)
# x - bday2 = x - bday1
# df = (x - meeting) / (x - bday1) - (x - meeting) / (x - bday2)
#  = (x - meeting) * (1 / (x - bday1) - 1 / (x - bday2))
# = (x - meeting) * (bday2 - bday1) / (x - bday2) / (x - bday1)

def fraction(x):
	a = (x - meeting).total_seconds()
	b = (x - benBday).total_seconds()
	c = (x - erinBday).total_seconds()
	return a / b - a / c

deltas = range(100)

dates = [meeting + timedelta(days=delta * 365) for delta in deltas]

fractions = [fraction(date) for date in dates]

for date, fraction in zip(dates, fractions):
	print date.isoformat(), "    ", fraction







# pbpaste | python